# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0


class SchemaException(Exception):
    """An exception that occurred while parsing a Schema"""

    pass

# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest

from fameio.source.schema.agenttype import AgentType
from fameio.source.schema.exception import SchemaException
from tests.utils import new_attribute, assert_exception_contains

AGENT_NAME = "DummyAgentName"


class TestAgentType:
    def test_init__name_missing__raises(self):
        with pytest.raises(SchemaException) as e_info:
            AgentType(None)  # noqa
        assert_exception_contains(AgentType._ERR_NAME_INVALID, e_info)

    def test_init__name_empty__raises(self):
        with pytest.raises(SchemaException) as e_info:
            AgentType("")  # noqa
        assert_exception_contains(AgentType._ERR_NAME_INVALID, e_info)

    def test_init__name_whitespace__raises(self):
        with pytest.raises(SchemaException) as e_info:
            AgentType(" ")  # noqa
        assert_exception_contains(AgentType._ERR_NAME_INVALID, e_info)

    def test_from_dict__no_definitions__returns_named__agent_type(self):
        result = AgentType.from_dict(AGENT_NAME, {})
        assert result.name == AGENT_NAME

    def test_from_dict__no_definitions__yields_empty_product_names(self):
        result = AgentType.from_dict(AGENT_NAME, {})
        assert result.get_product_names() == []

    def test_from_dict__no_definitions__yields_empty_attributes(self):
        result = AgentType.from_dict(AGENT_NAME, {})
        assert result.attributes == {}

    def test_from_dict__no_definitions__yields_empty_output(self):
        result = AgentType.from_dict(AGENT_NAME, {})
        assert result.outputs == {}

    def test_from_dict__no_definitions__yields_empty_metadata(self):
        result = AgentType.from_dict(AGENT_NAME, {})
        assert result.metadata == {}

    @pytest.mark.parametrize("products", ["", [], {}])
    def test_from_dict__products_empty__yields_empty_product_names(self, products):
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: products})
        assert result.get_product_names() == []

    def test_from_dict__products_not_list_or_dict__raises(self):
        with pytest.raises(SchemaException) as e_info:
            AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: "not a list or dict"})
        assert_exception_contains(AgentType._ERR_PRODUCTS_UNKNOWN_STRUCTURE, e_info)

    @pytest.mark.parametrize("products", [["a"], ["a", "b", "c"]])
    def test_from_dict__products_as_list__accept(self, products):
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: products})
        assert set(result.get_product_names()) == set(products)

    def test_from_dict__products_list_contains_other_than_string__raises(self):
        with pytest.raises(SchemaException) as e_info:
            AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: [5.0, "AA"]})
        assert_exception_contains(AgentType._ERR_PRODUCTS_NO_STRING, e_info)

    @pytest.mark.parametrize("products", [["a"], ["a", "b", "c"]])
    def test_products__from_list_input__is_converted_to_dict(self, products):
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: products})
        assert set(result.products.keys()) == set(products)

    @pytest.mark.parametrize("products", [{"a": 1}, {"a": "b", "c": "d"}, {"a": {"b": "c"}}])
    def test_from_dict__products_as_dict__accept(self, products):
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: products})
        assert result.products == products

    def test_from_dict__products_dict_contains_keys_other_than_string__raises(self):
        with pytest.raises(SchemaException) as e_info:
            AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: {5.0: "a", "a": "aa"}})
        assert_exception_contains(AgentType._ERR_PRODUCTS_NO_STRING, e_info)

    @pytest.mark.parametrize("products", [{"a": 1}, {"a": "b", "c": "d"}, {"a": {"b": "c"}}])
    def test_get_product_names__from_dict_input__is_converted_to_list(self, products):
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_PRODUCTS: products})
        assert set(result.get_product_names()) == set(products.keys())

    @pytest.mark.parametrize("outputs", [{}, {"a": 1}, {"a": "b", "c": "d"}, {"a": {"b": "c"}}])
    def test_from_dict__outputs__yield_outputs(self, outputs):
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_OUTPUTS: outputs})
        assert result.outputs == outputs

    @pytest.mark.parametrize("metadata", [{}, {"a": 1}, {"a": "b", "c": "d"}, {"a": {"b": "c"}}])
    def test_from_dict__metadata__yield_metadata(self, metadata):
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_METADATA: metadata})
        assert result.metadata == metadata

    @pytest.mark.parametrize("attribute_names", ["a", "b", "another_one"])
    def test_from_dict__attributes_from_dict__created(self, attribute_names):
        attributes = {name: new_attribute(name, "double", False, False, [])[name] for name in attribute_names}
        result = AgentType.from_dict(AGENT_NAME, {AgentType._KEY_ATTRIBUTES: attributes})
        assert set(result.attributes.keys()) == set(attribute_names)

# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest

from fameio.source.schema import Schema, SchemaException
from tests.utils import assert_exception_contains


class Test:
    def test_from_dict__empty__raises(self):
        with pytest.raises(SchemaException) as e_info:
            Schema.from_dict({})
        assert_exception_contains(Schema._AGENT_TYPES_MISSING, e_info)

    def test_from_dict__empty_agents__raises(self):
        with pytest.raises(SchemaException) as e_info:
            Schema.from_dict({Schema._KEY_AGENT_TYPE: {}})
        assert_exception_contains(Schema._AGENT_TYPES_EMPTY, e_info)

    def test_from_dict__to_dict__returns_identity(self):
        definitions = {Schema._KEY_AGENT_TYPE: {"Agent": {}, "OtherAgent": {}}}
        schema = Schema.from_dict(definitions)
        assert schema.to_dict() == definitions

    def test_from_string__of_to_string__returns_original_dict(self):
        definitions = {Schema._KEY_AGENT_TYPE: {"Agent": {}, "OtherAgent": {}}}
        schema = Schema.from_dict(definitions)
        string_representation = schema.to_string()
        second_schema = Schema.from_string(string_representation)
        assert second_schema.to_dict() == definitions

    def test_from_dict__mixed_caps_key_word__accepted_and_turned_lower_case(self):
        types = {"Agent": {}, "OtherAgent": {}}
        definitions = {"AgEntTYPeS": types}
        schema = Schema.from_dict(definitions)
        assert schema.to_dict() == {Schema._KEY_AGENT_TYPE: types}

    def test_from_dict__agent_types__contained_in_keys(self):
        types = {"Agent": {}, "OtherAgent": {}, "ThirdAgent": {}}
        schema = Schema.from_dict({Schema._KEY_AGENT_TYPE: types})
        assert set(schema.agent_types.keys()) == set(types.keys())

# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import logging
from pathlib import Path

import pytest
from mockito import expect, verifyNoUnwantedInteractions

from fameio.source.logs import (
    log,
    log_error_and_raise,
    set_up_logger,
    _WARN_ALREADY_INITIALIZED,  # noqa
    _FORMAT_DETAILLED,  # noqa
    _FORMAT_NORMAL,  # noqa
    _WARN_NOT_INITIALIZED,  # noqa
    _loggers,  # noqa
    LOG_LEVELS,
    logger,
    log_and_raise_critical,
    LOGGER_NAME,
)
from tests.utils import assert_log_contains, assert_exception_contains


@pytest.fixture
def clear_logs() -> None:
    _loggers.clear()
    yield
    _loggers.clear()


class TestLogs:
    def test_logger__uninitialised__logs_warning(self, caplog, clear_logs):
        caplog.set_level(logging.WARNING)
        logger()
        assert_log_contains(_WARN_NOT_INITIALIZED, caplog)

    def test_logger__uninitialised__returns_default_logger(self, clear_logs):
        assert logger() == log.getLogger(LOGGER_NAME)
        assert logger().level == log.INFO

    @pytest.mark.parametrize("ex", [ValueError("A"), IOError("ABC"), Exception("Help!")])
    def test_log_error_and_raise__logs_given_exception_as_str(self, ex: Exception, caplog, clear_logs):
        caplog.set_level(log.ERROR, LOGGER_NAME)
        with pytest.raises(Exception) as e_info:
            log_error_and_raise(ex)
        assert_log_contains(str(ex), caplog)
        assert_exception_contains(str(ex), e_info)

    def test_log_and_raise_critical__logs_message(self, caplog, clear_logs):
        caplog.set_level(log.CRITICAL, LOGGER_NAME)
        with pytest.raises(Exception) as e_info:
            log_and_raise_critical("MyMessage")
        assert_log_contains("MyMessage", caplog)
        assert_exception_contains("MyMessage", e_info)

    def test_set_up_logger__already_handled__logs_warning(self, caplog, clear_logs):
        set_up_logger("info")
        set_up_logger("error")
        assert_log_contains(_WARN_ALREADY_INITIALIZED, caplog)

    def test_set_up_logger__level_debug__detailed_format(self, clear_logs):
        with expect(log, times=1).Formatter(_FORMAT_DETAILLED, any):
            set_up_logger("debug")
        verifyNoUnwantedInteractions()

    @pytest.mark.parametrize("level", ["info", "warn", "error", "critical"])
    def test_set_up_logger__other_level__short_format(self, level, clear_logs):
        with expect(log, times=1).Formatter(_FORMAT_NORMAL, any):
            set_up_logger(level)
        verifyNoUnwantedInteractions()

    @pytest.mark.parametrize("level", ["info", "warn", "error", "critical", "debug"])
    def test_set_up_logger__log_level__is_set(self, level, clear_logs):
        set_up_logger(level)
        assert logger().getEffectiveLevel() == LOG_LEVELS[level]

    def test_set_up_logger__file_name_exists__adds_file_handler(self, clear_logs):
        set_up_logger("info", Path("deleteMe.ignore"))
        assert any([isinstance(handler, log.FileHandler) for handler in logger().handlers])

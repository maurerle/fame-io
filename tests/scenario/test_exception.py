# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import logging

import pytest

from fameio.source.logs import LOGGER_NAME
from fameio.source.scenario.exception import (
    log_and_raise,
    ScenarioException,
    get_or_raise,
    assert_or_raise,
    get_or_default,
    _DEFAULT_USED,  # noqa
)
from tests.utils import assert_log_contains, assert_logged_exception

_SAMPLE_MESSAGE = "This is a sample message"


def test_log_and_raise__raised_and_logged(caplog):
    caplog.set_level(logging.ERROR)
    with pytest.raises(ScenarioException) as e_info:
        log_and_raise(_SAMPLE_MESSAGE)
    assert_logged_exception(_SAMPLE_MESSAGE, caplog, e_info)


def test_get_or_raise__key_exists__returns_value():
    data = {"a": 2, "b": "c"}
    assert get_or_raise(data, "b", "IgnoredMessage") == "c"


def test_get_or_raise__key_missing_raises_and_logs(caplog):
    caplog.set_level(logging.ERROR)
    with pytest.raises(ScenarioException) as e_info:
        get_or_raise({}, "missing_key", _SAMPLE_MESSAGE)
    assert_logged_exception(_SAMPLE_MESSAGE, caplog, e_info)


def test_assert_or_raise__true__no_log(caplog):
    caplog.set_level(logging.DEBUG)
    assert_or_raise(True, _SAMPLE_MESSAGE)
    assert not caplog.text


def test_assert_or_raise__false__raises_and_logs(caplog):
    caplog.set_level(logging.ERROR)
    with pytest.raises(ScenarioException) as e_info:
        assert_or_raise(False, _SAMPLE_MESSAGE)
    assert_logged_exception(_SAMPLE_MESSAGE, caplog, e_info)


def test_get_or_default__key_exists__returns_value():
    data = {"a": 2, "b": "c"}
    assert get_or_default(data, "b", "Default") == "c"


def test_get_or_default__key_missing__returns_default():
    assert get_or_default({}, "missing_key", "Default") == "Default"


def test_get_or_default__key_missing__logs_missing(caplog):
    caplog.set_level(logging.DEBUG, LOGGER_NAME)
    get_or_default({}, "missing_key", "Default")
    assert_log_contains(_DEFAULT_USED, caplog)

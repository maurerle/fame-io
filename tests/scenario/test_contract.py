# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

from typing import List, Optional, Dict, Any

import pytest
from mockito import mock, expect, verifyNoUnwantedInteractions, unstub

from fameio.source.scenario import Attribute, Contract, ScenarioException

from tests.utils import assert_exception_contains


class CustomizedContract(Contract):
    def __init__(
        self,
        sender_id: int,
        receiver_id: int,
        product_name: str,
        delivery_interval: int,
        first_delivery_time: int,
        expiration_time: Optional[int] = None,
        meta_data: Optional[dict] = None,
    ) -> None:
        super().__init__(
            sender_id,
            receiver_id,
            product_name,
            delivery_interval,
            first_delivery_time,
            expiration_time,
            meta_data,
        )
        self._notified_count = 0

    def _notify_data_changed(self):
        self._notified_count += 1

    @property
    def notified_count(self) -> int:
        return self._notified_count


class TestContract:
    @staticmethod
    def contract_from_dict(
        sender=None, receiver=None, product=None, first=None, steps=None, expire=None, meta=None, attributes=None
    ) -> Contract:
        """Creates new dict from given input and a contract from it"""
        return Contract.from_dict(
            TestContract.define_contract(sender, receiver, product, first, steps, expire, meta, attributes)
        )

    @staticmethod
    def define_contract(
        sender=None, receiver=None, product=None, first=None, steps=None, expire=None, meta=None, attributes=None
    ) -> Dict[str, Any]:
        definition = {}
        if sender is not None:
            definition[Contract._KEY_SENDER] = sender
        if receiver is not None:
            definition[Contract._KEY_RECEIVER] = receiver
        if product is not None:
            definition[Contract._KEY_PRODUCT] = product
        if first is not None:
            definition[Contract._KEY_FIRST_DELIVERY] = first
        if steps is not None:
            definition[Contract._KEY_INTERVAL] = steps
        if expire is not None:
            definition[Contract._KEY_EXPIRE] = expire
        if meta is not None:
            definition[Contract._KEY_METADATA] = meta
        if attributes is not None:
            definition[Contract._KEY_ATTRIBUTES] = attributes
        return definition

    def test_from_dict__missing_sender__raises(self):
        with pytest.raises(ScenarioException) as e_info:
            self.contract_from_dict(None, 1, "ProdA", 0, 1)
        assert_exception_contains(Contract._ERR_MISSING_KEY, e_info)

    def test_from_dict__missing_receiver__raises(self):
        with pytest.raises(ScenarioException) as e_info:
            self.contract_from_dict(0, None, "ProdA", 0, 1)
        assert_exception_contains(Contract._ERR_MISSING_KEY, e_info)

    def test_from_dict__missing_product__raises(self):
        with pytest.raises(ScenarioException) as e_info:
            self.contract_from_dict(0, 1, None, 0, 1)
        assert_exception_contains(Contract._ERR_MISSING_KEY, e_info)

    def test_from_dict__missing_delivery_time__raises(self):
        with pytest.raises(ScenarioException) as e_info:
            self.contract_from_dict(0, 1, "ProdA", None, 1)
        assert_exception_contains(Contract._ERR_MISSING_KEY, e_info)

    def test_from_dict__missing_delivery_interval__raises(self):
        with pytest.raises(ScenarioException) as e_info:
            self.contract_from_dict(0, 1, "ProdA", 0, None)
        assert_exception_contains(Contract._ERR_MISSING_KEY, e_info)

    def test_from_dict__base_values__set(self):
        contract = self.contract_from_dict(0, 1, "ProdA", 22, 33)
        assert contract.sender_id == 0
        assert contract.receiver_id == 1
        assert contract.product_name == "ProdA"
        assert contract.first_delivery_time == 22
        assert contract.delivery_interval == 33

    def test_from_dict__unset_values_are_none(self):
        contract = self.contract_from_dict(0, 1, "ProdA", 0, 1)
        assert contract.expiration_time is None
        assert contract.meta_data is None
        assert contract.attributes == {}

    def test_from_dict__expiration_time__set(self):
        contract = self.contract_from_dict(0, 1, "ProdA", 0, 1, expire=2000)
        assert contract.expiration_time == 2000

    def test_from_dict__meta_data__set(self):
        contract = self.contract_from_dict(0, 1, "ProdA", 0, 1, meta={"Description": "This is meta data"})
        assert isinstance(contract.meta_data, dict)
        assert contract.meta_data["Description"] == "This is meta data"

    def test_from_dict__attributes__set(self):
        attributes = {"A": 1, "B": 2.2, "C": "SomeString"}
        contract = self.contract_from_dict(0, 1, "ProdA", 0, 1, attributes=attributes)
        for attribute, value in attributes.items():
            assert attribute in contract.attributes
            assert contract.attributes[attribute].value == value

    def test_split_contract_definitions__one_to_one__returns_same(self):
        definition = self.define_contract(0, 1, "ProdA", 0, 1)
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 1
        assert result[0] == definition

    def test_split_contract_definitions__missing_sender__raises(self):
        definition = self.define_contract(None, 1, "ProdA", 0, 1)
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._ERR_MISSING_KEY, e_info)

    def test_split_contract_definitions__missing_receiver__raises(self):
        definition = self.define_contract(0, None, "ProdA", 0, 1)
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._ERR_MISSING_KEY, e_info)

    def test_split_contract_definitions__m_to_n__raises(self):
        definition = self.define_contract([0, 1, 2], [1, 2, 3, 4], "ProdA", 0, 1)
        with pytest.raises(ScenarioException) as e_info:
            Contract.split_contract_definitions(definition)
        assert_exception_contains(Contract._ERR_MULTI_CONTRACT_CORRUPT, e_info)

    def test_split_contract_definitions__one_to_n__creates_list(self):
        definition = self.define_contract([0, 1, 2], 5, "ProdA", 0, 1)
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([0, 1, 2], [5, 5, 5], result)

    @staticmethod
    def assert_contract_dicts_match(contracts: List[dict]) -> None:
        """Asserts that for given list of `contracts`, all values except for keys 'senderId' & 'receiverId' match"""
        for index in range(1, len(contracts)):
            for key in [
                Contract._KEY_PRODUCT,
                Contract._KEY_FIRST_DELIVERY,
                Contract._KEY_INTERVAL,
                Contract._KEY_EXPIRE,
                Contract._KEY_ATTRIBUTES,
                Contract._KEY_METADATA,
            ]:
                if key in contracts[0]:
                    assert contracts[index][key] == contracts[0][key]

    @staticmethod
    def assert_contract_x_to_y(senders: List[int], receivers: List[int], contracts: List[dict]) -> None:
        """ "Asserts that the n-th item in `contracts` matches the n-th of `senders` and `receivers`"""
        for index in range(len(contracts)):
            assert contracts[index][Contract._KEY_SENDER] == senders[index]
            assert contracts[index][Contract._KEY_RECEIVER] == receivers[index]

    def test_split_contract_definitions__n_to_one__creates_list(self):
        definition = self.define_contract(0, [5, 6, 7], "ProdA", 0, 1)
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([0, 0, 0], [5, 6, 7], result)

    def test_split_contract_definitions__n_to_n__creates_list(self):
        definition = self.define_contract([1, 2, 3], [5, 6, 7], "ProdA", 0, 1)
        result = Contract.split_contract_definitions(definition)
        assert len(result) == 3
        TestContract.assert_contract_dicts_match(result)
        TestContract.assert_contract_x_to_y([1, 2, 3], [5, 6, 7], result)

    def test_init__values_set(self):
        c = Contract(
            sender_id=10,
            receiver_id=20,
            product_name="ProductName",
            delivery_interval=100,
            first_delivery_time=200,
        )
        assert c.sender_id == 10
        assert c.display_sender_id == "#10"
        assert c.receiver_id == 20
        assert c.display_receiver_id == "#20"
        assert c.product_name == "ProductName"
        assert c.delivery_interval == 100
        assert c.first_delivery_time == 200
        assert c.expiration_time is None
        assert c.meta_data is None
        assert len(c.attributes) == 0

    def test_init__negative_interval__raises(self):
        with pytest.raises(ValueError) as e_info:
            Contract(sender_id=10, receiver_id=20, product_name="p", delivery_interval=-2, first_delivery_time=0)
        assert_exception_contains(Contract._ERR_INTERVAL_NOT_POSITIVE, e_info)

    def test_init__zero_interval_raises(self):
        with pytest.raises(ValueError) as e_info:
            Contract(sender_id=10, receiver_id=20, product_name="p", delivery_interval=0, first_delivery_time=0)
        assert_exception_contains(Contract._ERR_INTERVAL_NOT_POSITIVE, e_info)

    def test_add_attribute__calls_notify_data_changed(self):
        contract = self.contract_from_dict(0, 1, "ProdA", 0, 1)
        with expect(Contract, times=1)._notify_data_changed():
            contract.add_attribute("MyAttribute", mock(Attribute))
        verifyNoUnwantedInteractions()
        unstub()

    def test_add_attribute__double_definition__raises(self):
        contract = self.contract_from_dict(0, 1, "ProdA", 0, 1)
        contract.add_attribute("MyAttribute", mock(Attribute))
        with pytest.raises(ValueError) as e_info:
            contract.add_attribute("MyAttribute", mock(Attribute))
        assert_exception_contains(Contract._ERR_DOUBLE_ATTRIBUTE, e_info)

    def test_notify_add_attribute(self):
        a = CustomizedContract(
            sender_id=10,
            receiver_id=20,
            product_name="ProductName",
            delivery_interval=100,
            first_delivery_time=200,
        )
        assert len(a.attributes) == 0
        assert a.notified_count == 0
        a.add_attribute("attr_name", Attribute("attr_full_name", 10))
        assert len(a.attributes) == 1
        assert a.notified_count == 1

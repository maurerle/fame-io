# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest
from fameio.source.scenario import Attribute, Scenario, ScenarioException

from tests.utils import assert_exception_contains, new_agent, new_schema


def new_scenario(
    schema: dict = None,
    general: dict = None,
    agents: list = None,
    contracts: list = None,
) -> Scenario:
    """Creates a new scenario from the given `schema`, `general` dicts plus `agents` and `contract lists`"""
    scenario = {}
    if schema:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_SCHEMA] = schema
    if general:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_GENERAL] = general
    if agents:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_AGENTS] = agents
    if contracts:
        # noinspection PyProtectedMember
        scenario[Scenario._KEY_CONTRACTS] = contracts
    return Scenario.from_dict(scenario)


class TestScenario:
    schema = new_schema([new_agent("Agent", [], [])])
    general = {"RunId": 1, "Simulation": {"StartTime": 0, "StopTime": 0}}

    def test_init_missing_schema(self):
        with pytest.raises(ScenarioException) as e_info:
            new_scenario(None, dict(), list(), list())
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test_init_missing_general(self):
        with pytest.raises(ScenarioException) as e_info:
            new_scenario(TestScenario.schema, None, list(), list())
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test_accept_no_agent(self):
        new_scenario(TestScenario.schema, TestScenario.general, None, list())

    def test_accept_no_contract(self):
        new_scenario(TestScenario.schema, TestScenario.general, list(), None)


class TestAttribute:
    def test_init_missing_value(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", None)
        assert_exception_contains(Attribute._VALUE_MISSING, e_info)

    def test_init_zero_not_missing_value(self):
        assert Attribute("AnAttribute", 0) is not None

    def test_init_single_value(self):
        value = 5
        attribute = Attribute("MyAttribute", value)
        assert attribute.has_nested is False
        assert attribute.value == value

    def test_init_list(self):
        value = [5.0, 7.0, 9.9]
        attribute = Attribute("MyAttribute", value)
        assert attribute.has_nested is False
        assert attribute.value == value

    def test_init_nested_fail_missing_value(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", {"InnerAttrib": None})
        assert_exception_contains(Attribute._VALUE_MISSING, e_info)

    def test_init_nested(self):
        value = "MyInnerValue"
        attribute = Attribute("AnAttribute", {"InnerAttrib": value})
        assert attribute.has_nested is True
        assert attribute.value is None
        inner_attribute = attribute.get_nested_by_name("InnerAttrib")
        assert inner_attribute.value == value

    def test_init_empty_list(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", [])
        assert_exception_contains(Attribute._LIST_EMPTY, e_info)

    def test_init_empty_dict(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", {})
        assert_exception_contains(Attribute._DICT_EMPTY, e_info)

    def test_init_nested_list(self):
        attribute = Attribute("AnAttribute", [{"A": 1, "B": 2}, {"A": 10, "B": 20}])
        assert attribute.has_nested_list
        assert not attribute.has_nested
        assert not attribute.has_value
        nested_list = attribute.nested_list
        assert len(nested_list) == 2
        assert nested_list[0]["A"].value == 1
        assert nested_list[1]["A"].value == 10

    def test_init_nested_list_inconsistent_data(self):
        with pytest.raises(ScenarioException) as e_info:
            Attribute("AnAttribute", [{"A": 1, "B": 2}, 5])
        assert_exception_contains(Attribute._MIXED_DATA, e_info)

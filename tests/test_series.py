# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import logging
import os

import pandas as pd
import pytest
from mockito import when, ANY  # noqa

from fameio.source.series import TimeSeriesManager, TimeSeriesException
from tests.utils import assert_logged_exception


class TestTimeSeriesManager:
    _DUMMY_FRAME_VALID = pd.DataFrame(data={0: [0], 1: [1]})

    @pytest.fixture
    def manager(self):
        return TimeSeriesManager()

    @pytest.mark.parametrize("values", [[0, 0], [2, 2], [2.0, 2], [-1, -1], [2000.0, 2000], [2, 2, 2, 2, 2]])
    def test_register_and_validate_same_numeric_identifiers__yield_same_id(self, values, manager):
        manager.register_and_validate(values[0])
        ids = [manager.get_series_id_by_identifier(i) for i in values]
        assert len(set(ids)) == 1

    @pytest.mark.parametrize("values", [["a", "a"], ["my/file/path.csv", "my/file/path.csv"]])
    def test_register_and_validate_same_string_identifiers__yield_same_id(self, manager, values):
        with when(pd).read_csv(ANY, sep=";", header=None).thenReturn(self._DUMMY_FRAME_VALID), when(os.path).exists(
            ANY
        ).thenReturn(True):
            for value in values:
                manager.register_and_validate(value)
        ids = [manager.get_series_id_by_identifier(i) for i in values]
        assert len(set(ids)) == 1

    @pytest.mark.parametrize("values", [[2, 3], [-1, 1], [0, 0.5, 1, 2, 4]])
    def test_register_and_validate__different_numeric_identifiers__yield_different_id(self, values, manager):
        for value in values:
            manager.register_and_validate(value)
        id_set = set([manager.get_series_id_by_identifier(value) for value in values])
        assert len(id_set) == len(values), "Not all created IDs are different"

    def test_register_and_validate__missing_file_raises(self, manager, caplog):
        with when(os.path).exists(ANY).thenReturn(False), pytest.raises(TimeSeriesException) as e_info:
            manager.register_and_validate("missing file")
        assert_logged_exception(TimeSeriesManager._ERR_FILE_NOT_FOUND, caplog, e_info)

    @pytest.mark.parametrize(
        "value, msg",
        [
            ({0: ["NotATimeStamp"], 1: [1]}, TimeSeriesManager._ERR_CORRUPT_TIME_SERIES_KEY),
            ({0: [1], 1: ["NotNumeric"]}, TimeSeriesManager._ERR_CORRUPT_TIME_SERIES_VALUE),
            ({0: [1], 1: [float("NaN")]}, TimeSeriesManager._ERR_NAN_VALUE),
            ({0: [1, 2, 3], 1: [1.0, None, 2.0]}, TimeSeriesManager._ERR_NAN_VALUE),
        ],
    )
    def test_register_and_validate__invalid_series__raises(self, caplog, manager, value, msg: str):
        caplog.set_level(logging.ERROR)
        with when(pd).read_csv(ANY, sep=";", header=None).thenReturn(pd.DataFrame(data=value)), when(os.path).exists(
            ANY
        ).thenReturn(True), pytest.raises(TimeSeriesException) as e_info:
            manager.register_and_validate("file")
        assert_logged_exception(msg, caplog, e_info)

    def test_get_unique_timeseries_id__nan_identifier__raises(self, caplog, manager):
        caplog.set_level(logging.ERROR)
        with pytest.raises(TimeSeriesException) as e_info:
            manager.register_and_validate(float("NaN"))
        assert_logged_exception(TimeSeriesManager._ERR_NAN_VALUE, caplog, e_info)

    def test_get_series_id_by_identifier(self, caplog, manager):
        caplog.set_level(logging.ERROR)
        with pytest.raises(TimeSeriesException) as e_info:
            manager.get_series_id_by_identifier("not yet registered")
        assert_logged_exception(TimeSeriesManager._ERR_UNREGISTERED_SERIES, caplog, e_info)

    def test_get_all_series__all_ids_contained(self, manager):
        manager.register_and_validate(3.0)
        manager.register_and_validate(99)
        ids = [i for i, _, _ in manager.get_all_series()]
        assert len(set(ids)) == 2

    def test_get_all_series__names_match_file_or_creation_pattern(self, manager):
        manager.register_and_validate(3)
        with when(pd).read_csv(ANY, sep=";", header=None).thenReturn(self._DUMMY_FRAME_VALID), when(os.path).exists(
            ANY
        ).thenReturn(True):
            manager.register_and_validate("myFile.csv")

        names = [name for _, name, _ in manager.get_all_series()]
        assert TimeSeriesManager._CONSTANT_IDENTIFIER.format(3) in names
        assert "myFile.csv" in names

    def test_get_all_series__series_created_from_constant__match_format(self, manager):
        manager.register_and_validate(3.3)
        _, _, series = manager.get_all_series()[0]
        assert series[0][0] == 0
        assert series[1][0] == 3.3

    def test_get_all_series__series_from_file__match_format(self, manager):
        with when(pd).read_csv(ANY, sep=";", header=None).thenReturn(self._DUMMY_FRAME_VALID), when(os.path).exists(
            ANY
        ).thenReturn(True):
            manager.register_and_validate("myFile.csv")
        _, _, series = manager.get_all_series()[0]
        assert series[0][0] == 0
        assert series[1][0] == 1

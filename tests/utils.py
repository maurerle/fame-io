# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

from typing import Any, Union

import pytest
from _pytest._code import ExceptionInfo  # noqa


def assert_logged_exception(expected: str, log: pytest.LogCaptureFixture, e_info: ExceptionInfo) -> None:
    """Asserts that `expected` string is contained in given `log` and `e_info`"""
    assert_log_contains(expected, log)
    assert_exception_contains(expected, e_info)


def assert_log_contains(expected: str, log: pytest.LogCaptureFixture) -> None:
    """Asserts that given `log` text contains given `expected` string - ignoring any placeholders"""
    assert_exception_contains(expected, log.text)


def assert_exception_contains(expected: str, e_info: Union[ExceptionInfo, str]):
    """Asserts that the given ExceptionInfo `e_info` contains given `expected` string - ignoring any placeholders"""
    removed_placeholders = expected.split(sep="{}")
    for message_part in removed_placeholders:
        assert message_part in str(e_info)


def assert_dicts_equal(ref: dict, test: dict, path: str = "") -> None:
    """Asserts that given dicts are fully equal - ignoring their order and also testing possible inner dicts"""
    for ref_key, ref_value in ref.items():
        assert ref_key in test
        element = path + ref_key
        test_value = test[ref_key]
        if isinstance(ref_value, dict):
            assert_dicts_equal(ref_value, test_value, element + ":")
        else:
            assert ref_value == test_value, f"Expected value for element {element} is {ref_value} but was: {test_value}"


def new_schema(agent_list: list) -> dict:
    """Returns a schema with given agent definitions"""
    types = {"AgentTypes": {}}
    for entry in agent_list:
        for agent_name, agent_definition in entry.items():
            types["AgentTypes"][agent_name] = agent_definition
    return types


def new_agent(name: str, attribute_list: list, products: Any) -> dict:
    """Returns a schema dict for agent with given `name`, `attributes` and `products`"""
    agent = {name: {"Attributes": {}}}
    if products:
        agent[name]["Products"] = products

    for entry in attribute_list:
        for attribute_name, attribute_description in entry.items():
            agent[name]["Attributes"][attribute_name] = attribute_description
    return agent


def new_attribute(
    name: str,
    data_type: str = None,
    is_mandatory: bool = None,
    is_list: bool = None,
    nested_list: list = None,
    values: Any = None,
    default: str = None,
    help_str: str = None,
) -> dict:
    """Returns a schema dict for a single attribute (with possible nested attributes)"""
    attribute = {
        name: {
            "AttributeType": data_type,
            "Mandatory": is_mandatory,
            "List": is_list,
            "NestedAttributes": {},
            "Default": default,
        }
    }

    # necessary to avoid type checker warnings
    if not data_type:
        del attribute[name]["AttributeType"]
    if is_mandatory is None:
        del attribute[name]["Mandatory"]
    if is_list is None:
        del attribute[name]["List"]
    if not default:
        del attribute[name]["Default"]

    if not nested_list:
        del attribute[name]["NestedAttributes"]
    else:
        for entry in nested_list:
            for nested_name, nested_description in entry.items():
                attribute[name]["NestedAttributes"][nested_name] = nested_description

    if values is not None:
        if len(values) > 0:
            attribute[name]["Values"] = values
        else:
            attribute[name]["Values"] = None

    if help_str is not None:
        attribute[name]["Help"] = help_str

    return attribute

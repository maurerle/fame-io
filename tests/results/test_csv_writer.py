# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import os
from pathlib import Path
from typing import List

import pandas as pd

from fameio.source.results.csv_writer import CsvWriter
from fameio.source.results.data_transformer import INDEX


def create_data_frame(agents: int, times: int, columns: List[str]) -> pd.DataFrame:
    """
    Creates a dataframe with given count of `agents`, each with `times` rows and given `columns`
    Each value is the product of agent_id, time and column index
    """
    output = {
        (agent_id, time): {column_name: agent_id * time * i for i, column_name in enumerate(columns)}
        for agent_id in range(agents)
        for time in range(times)
    }
    data_frame = pd.DataFrame.from_dict(output, orient="index")
    data_frame.rename_axis(INDEX, inplace=True)
    return data_frame


def create_data_frame_with_additional_index_column(
    agents: int, times: int, columns: List[str], idx_col: str
) -> pd.DataFrame:
    """
    Creates a dataframe with MultiIndex ("AgentId", "TimeStep", `idx_col`) with given count of `agents`, each with `times` rows and given `columns`
    Each value is the product of agent_id, time and column index
    """
    output = {
        (agent_id, time, idx_col): {column_name: agent_id * time * i for i, column_name in enumerate(columns)}
        for agent_id in range(agents)
        for time in range(times)
    }
    data_frame = pd.DataFrame.from_dict(output, orient="index")
    data_frame.rename_axis(INDEX + (idx_col,), inplace=True)
    return data_frame


def assert_exists_and_matching(expected_file: Path, expected_data: pd.DataFrame) -> None:
    """Asserts that given `expected_file` exists and contains identical data to given dataframe `compare_to`"""
    assert expected_file.exists()
    parsed_df = pd.read_csv(expected_file, sep=";", index_col=INDEX)
    pd.testing.assert_frame_equal(expected_data, parsed_df)


class TestCsvWriter:
    def test_init_creates_specified_folder(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        file = Path(tmp_path, "myResult.pb")
        CsvWriter(directory, file, False)
        assert directory.is_dir()

    def test_init_no_path_creates_folder_in_working_dir_from_filename(self, tmp_path):
        file = Path("/this/path/is/not/used/myResult.pb")
        os.chdir(tmp_path)
        CsvWriter(None, file, False)
        assert (Path(tmp_path, "myResult")).is_dir()

    def test_init_keeps_existing_output_folder(self, tmp_path):
        file = Path(tmp_path, "myResult.pb")
        directory = Path(tmp_path, "myFolder")
        directory.mkdir()
        file_to_keep = Path(directory, "keep_this.txt")
        file_to_keep.write_text("Hello Mama")
        CsvWriter(directory, file, False)
        assert file_to_keep.exists()

    def test_write_to_files_multi_export_simple_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        assert_exists_and_matching(Path(directory, "myAgent.csv"), df)

    def test_write_to_files_multi_export_complex_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        df2 = create_data_frame(2, 3, ["A2", "B2", "C2"])
        csv_writer.write_to_files("myAgent", {None: df, "Complex": df2})
        assert_exists_and_matching(Path(directory, "myAgent.csv"), df)
        assert_exists_and_matching(Path(directory, "myAgent_Complex.csv"), df2)

    def test_write_to_files_single_export_simple_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        for agent_id, agent_data in df.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_{agent_id}.csv"), agent_data)

    def test_write_to_files_single_export_complex_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        df2 = create_data_frame(3, 5, ["A2", "B2", "C2"])
        csv_writer.write_to_files("myAgent", {None: df, "Complex": df2})
        for agent_id, agent_data in df.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_{agent_id}.csv"), agent_data)
        for agent_id, agent_data in df2.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_Complex_{agent_id}.csv"), agent_data)

    def test_write_to_files_multi_export_append(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        expected_result = pd.concat([df, df2])
        assert_exists_and_matching(Path(directory, "myAgent.csv"), expected_result)

    def test_write_to_files_single_export_append(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        expected_result = pd.concat([df, df2])
        for agent_id, agent_data in expected_result.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_{agent_id}.csv"), agent_data)

    def test_write_to_files_overwrite_existing_multi_export(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})

        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        assert_exists_and_matching(directory / "myAgent.csv", df2)

    def test_write_to_files_overwrite_existing_single_export(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})

        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        for agent_id, agent_data in df2.groupby(INDEX[0]):
            assert_exists_and_matching(directory / f"myAgent_{agent_id}.csv", agent_data)

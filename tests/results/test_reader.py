# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import io
import logging
import logging as log
from typing import IO, Tuple, List

import pytest
from fameprotobuf.DataStorage_pb2 import DataStorage

from fameio.source.logs import LOGGER_NAME
from fameio.source.results.reader import Reader, ReaderV0, ReaderV1
from tests.utils import assert_exception_contains

RANDOM_BINARY = b"pafdgoasdeASFLkmsefmgdso3"  # noqa
EMPTY_BINARY = b""
HEADER_V1 = b"famecoreprotobufstreamfilev001"  # noqa


def make_storage_string() -> bytes:
    """Returns serialised DataStorage"""
    data_storage = DataStorage()
    output = data_storage.output
    agent_type = output.agentType.add()
    agent_type.className = "MyAgent"
    return data_storage.SerializeToString()


class DummyReader(Reader):
    """For testing of ABC methods"""

    def read(self):
        pass


class TestReader:
    def test_get_reader_with_header_v1(self):
        file = io.BytesIO(HEADER_V1)
        assert isinstance(Reader.get_reader(file), ReaderV1)

    def test_get_reader_without_header_v0(self, caplog):
        caplog.set_level(logging.WARN)
        file = io.BytesIO(b"notTheHeader")
        assert isinstance(Reader.get_reader(file), ReaderV0)
        assert Reader._WARN_NO_HEADER in caplog.text

    def test_get_reader_with_header_empty_v0(self, caplog):
        caplog.set_level(logging.WARN)
        file = io.BytesIO(EMPTY_BINARY)
        assert isinstance(Reader.get_reader(file), ReaderV0)
        assert Reader._WARN_NO_HEADER in caplog.text

    def test_get_reader_with_header_random_v0(self, caplog):
        caplog.set_level(logging.WARN)
        file = io.BytesIO(RANDOM_BINARY)
        assert isinstance(Reader.get_reader(file), ReaderV0)
        assert Reader._WARN_NO_HEADER in caplog.text

    def test_get_reader_v0_resets_file_pointer(self):
        file = io.BytesIO(RANDOM_BINARY)
        Reader.get_reader(file)
        assert file.tell() == 0

    def test_get_reader_v1_single_mode(self):
        file = io.BytesIO(HEADER_V1)
        reader = Reader.get_reader(file, read_single=True)
        assert reader._read_single

    def test_get_reader_v0_single_mode_logs_error(self, caplog):
        file = io.BytesIO(EMPTY_BINARY)
        reader = Reader.get_reader(file, read_single=True)
        assert not reader._read_single
        assert Reader._ERR_UNSUPPORTED_MODE in caplog.text

    def test_read_message_length_end_of_file(self, caplog):
        caplog.set_level(log.DEBUG, LOGGER_NAME)
        file = io.BytesIO(EMPTY_BINARY)
        message_length = DummyReader(file, False)._read_message_length()
        assert Reader._DEBUG_FILE_END_REACHED in caplog.text
        assert message_length == 0

    def test_read_message_length_positive(self):
        binary_string = (555).to_bytes(4, byteorder="big", signed=True)
        file = io.BytesIO(binary_string)
        message_length = DummyReader(file, False)._read_message_length()
        assert 555 == message_length

    def test_read_message_length_file_pointer_incremented(self):
        binary_string = (555).to_bytes(4, byteorder="big", signed=True)
        file = io.BytesIO(binary_string + b"some_random_stuff")
        DummyReader(file, False)._read_message_length()
        assert file.tell() == Reader._BYTES_DEFINING_MESSAGE_LENGTH

    def test_read_data_storage_message_length_negative_length(self):
        file = io.BytesIO(b"lelalu")  # noqa
        with pytest.raises(IOError) as e_info:
            DummyReader(file, False)._read_data_storage_message(-2)
        assert_exception_contains(Reader._ERR_FILE_CORRUPT_NEGATIVE_LENGTH, e_info)

    def test_read_data_storage_message_missing_data_log_error(self, caplog):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string)
        message_length = len(binary_string) + 100
        DummyReader(file, False)._read_data_storage_message(message_length)
        assert Reader._ERR_FILE_CORRUPT_MISSING_DATA in caplog.text

    def test_read_data_storage_message_no_length_read_all(self):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string)
        DummyReader(file, False)._read_data_storage_message()
        assert len(binary_string) == file.tell()

    def test_read_data_storage_message_read_given_number_of_bytes(self):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string + b"otherStuffNotToBeRead")
        DummyReader(file, False)._read_data_storage_message(len(binary_string))
        assert len(binary_string) == file.tell()

    def test_read_data_storage_message_returns_none(self):
        file = io.BytesIO(EMPTY_BINARY)
        assert DummyReader(file, False).read() is None


class TestReaderV0:
    def test_read_returns_list_with_one_entry(self):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string)
        result = ReaderV0(file, False).read()
        assert len(result) == 1
        assert file.tell() == len(binary_string)
        assert isinstance(result[0], DataStorage)

    def test_read_second_time_returns_empty_list(self):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string)
        ReaderV0(file, False).read()
        result = ReaderV0(file, False).read()
        assert len(result) == 0

    def test_init_logs_deprecation_warning(self, caplog):
        caplog.set_level(logging.WARN)
        file = io.BytesIO(EMPTY_BINARY)
        ReaderV0(file, False)
        assert ReaderV0._WARN_DEPRECATED in caplog.text


class TestReaderV1:
    def test_read_broken_message(self):
        lengths, file = self._build_data_storage_binary_stream(1, RANDOM_BINARY)
        with pytest.raises(IOError) as e_info:
            ReaderV1(file, False).read()
        assert_exception_contains(Reader._ERR_PARSING_FAILED, e_info)

    def test_read_one_message(self):
        lengths, file = self._build_data_storage_binary_stream(1, make_storage_string())
        result = ReaderV1(file, False).read()
        assert len(result) == 1
        assert file.tell() == lengths[0] + Reader._BYTES_DEFINING_MESSAGE_LENGTH
        assert isinstance(result[0], DataStorage)

    def test_read_multiple_messages(self):
        lengths, file = self._build_data_storage_binary_stream(3, make_storage_string())
        result = ReaderV1(file, False).read()
        assert len(result) == 3
        assert file.tell() == 3 * Reader._BYTES_DEFINING_MESSAGE_LENGTH + sum(lengths)
        assert all([isinstance(item, DataStorage) for item in result])

    @staticmethod
    def _build_data_storage_binary_stream(number_of_storages: int, message: bytes) -> Tuple[List[int], IO]:
        """Returns a binary stream of one or multiple data storages (prefixed by their byte length)"""
        stream = b""
        lengths = []
        for count in range(0, number_of_storages):
            stream += len(message).to_bytes(4, byteorder="big", signed=True)
            stream += message
            lengths.append(len(message))
        return lengths, io.BytesIO(stream)

    def test_read_in_memory_saving_mode_returns_one(self):
        lengths, file = self._build_data_storage_binary_stream(3, make_storage_string())
        result = ReaderV1(file, True).read()
        assert len(result) == 1
        assert file.tell() == Reader._BYTES_DEFINING_MESSAGE_LENGTH + lengths[0]

    def test_read_in_memory_saving_mode_returns_empty_list_at_end(self):
        _, file = self._build_data_storage_binary_stream(1, make_storage_string())
        reader = ReaderV1(file, True)
        reader.read()
        result = reader.read()
        assert len(result) == 0

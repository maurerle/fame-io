# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pandas as pd
import pytest

import fameio
from fameio.source.cli import MergingOptions
from fameio.source.results.conversion import (
    apply_time_option,
    merge_time,
    apply_time_merging,
)
from fameio.source.time import ConversionException
from tests.results.test_csv_writer import create_data_frame, create_data_frame_with_additional_index_column
from fameio.source.results.data_transformer import INDEX
from pandas.testing import assert_frame_equal

from tests.utils import assert_exception_contains


class TestTimeConversion:
    @pytest.mark.parametrize("mode_name", ["utc", "UTC", "UtC", "int", "fame"])
    def test_apply_time_option_utc(self, mode_name):
        data = {None: create_data_frame(agents=1, times=3, columns=["A", "B", "C"])}
        apply_time_option(data, mode_name=mode_name)

    @pytest.mark.parametrize("mode_name", ["invalid_mode", "lalelu", "UTCA", "FAMES", "uint"])
    def test_apply_time_option_invalid(self, mode_name):
        data = {None: create_data_frame(agents=1, times=3, columns=["A", "B", "C"])}
        with pytest.raises(ConversionException) as e_info:
            apply_time_option(data, mode_name=mode_name)
        assert_exception_contains(fameio.source.results.conversion._ERR_UNIMPLEMENTED, e_info)

    def test_convert_time_index_utc(self):
        data = {None: create_data_frame(agents=1, times=3, columns=["A"])}
        expected = pd.DataFrame.from_dict(
            {
                (0, "2000-01-01 00:00:00"): {"A": 0},
                (0, "2000-01-01 00:00:01"): {"A": 0},
                (0, "2000-01-01 00:00:02"): {"A": 0},
            },
            orient="index",
        ).rename_axis(INDEX)
        fameio.source.results.conversion._convert_time_index(data, datetime_format="%Y-%m-%d %H:%M:%S")
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    def test_convert_time_index_fame(self):
        data = {None: create_data_frame(agents=1, times=3, columns=["A"])}
        expected = pd.DataFrame.from_dict(
            {
                (0, "2000-01-01_00:00:00"): {"A": 0},
                (0, "2000-01-01_00:00:01"): {"A": 0},
                (0, "2000-01-01_00:00:02"): {"A": 0},
            },
            orient="index",
        ).rename_axis(INDEX)
        fameio.source.results.conversion._convert_time_index(data, datetime_format="%Y-%m-%d_%H:%M:%S")
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    def test_convert_time_index_multiindex_fame(self):
        data = {None: create_data_frame_with_additional_index_column(1, 3, ["A"], "Plant")}
        expected = pd.DataFrame.from_dict(
            {
                (0, "2000-01-01_00:00:00", "Plant"): {"A": 0},
                (0, "2000-01-01_00:00:01", "Plant"): {"A": 0},
                (0, "2000-01-01_00:00:02", "Plant"): {"A": 0},
            },
            orient="index",
        ).rename_axis((["AgentId", "TimeStep", "Plant"]))
        fameio.source.results.conversion._convert_time_index(data, datetime_format="%Y-%m-%d_%H:%M:%S")
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    @pytest.mark.parametrize(
        "sample",
        [
            (0, 0, 0, 1, 0),
            (17, 0, 0, 1, 17),
            (-17, 0, 0, 1, -17),
            (8, 0, 5, 10, 10),
            (5, 0, 5, 10, 10),
            (14, 0, 5, 10, 10),
            (24, 0, 5, 10, 20),
            (26, 0, 5, 10, 30),
            (-7, 2, 0, 3, -7),
            (-6, 2, 0, 3, -7),
            (-8, 2, 0, 3, -10),
            (-1, 2, 0, 3, -1),
            (-2, 2, 0, 3, -4),
            (0, 2, 0, 3, -1),
            (1, 2, 0, 3, -1),
            (2, 2, 0, 3, 2),
            (3, 2, 0, 3, 2),
            (4, 2, 0, 3, 2),
            (5, 2, 0, 3, 5),
            (4, 2, 1, 3, 5),
            (5, 2, 1, 3, 5),
            (6, 2, 1, 3, 5),
            (-6, 2, 2, 3, -4),
            (-7, 2, 2, 3, -7),
            (-8, 2, 2, 3, -7),
            (-6, 2, 1, 3, -7),
            (-7, 2, 1, 3, -7),
            (-8, 2, 1, 3, -7),
        ],
    )
    def test_merge_time(self, sample):
        t, focal_point, offset, interval, expected = sample
        assert merge_time(t, focal_point, offset, interval) == expected

    def test_apply_time_merging(self):
        data = {None: create_data_frame(agents=2, times=3, columns=["A", "B"])}
        config = {MergingOptions.STEPS_BEFORE: 1, MergingOptions.STEPS_AFTER: 0, MergingOptions.FOCAL_POINT: 2}
        apply_time_merging(data, config)
        expected = pd.DataFrame.from_dict(
            {
                (0, 0): {"A": 0, "B": 0},
                (0, 2): {"A": 0, "B": 0},
                (1, 0): {"A": 0, "B": 0},
                (1, 2): {"A": 0, "B": 2 + 1},
            },
            orient="index",
        ).rename_axis(INDEX)
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    def test_apply_time_merging_no_config(self):
        data = {None: create_data_frame(agents=2, times=3, columns=["A", "B"])}
        config = None
        apply_time_merging(data, config)
        expected = pd.DataFrame.from_dict(
            {
                (0, 0): {"A": 0, "B": 0},
                (0, 1): {"A": 0, "B": 0},
                (0, 2): {"A": 0, "B": 0},
                (1, 0): {"A": 0, "B": 0},
                (1, 1): {"A": 0, "B": 1},
                (1, 2): {"A": 0, "B": 2},
            },
            orient="index",
        ).rename_axis(INDEX)
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    def test_apply_time_merging_empty_df(self):
        index = pd.MultiIndex.from_arrays([[], []], names=INDEX)
        data = {None: pd.DataFrame(index=index)}
        config = {MergingOptions.STEPS_BEFORE: 1, MergingOptions.STEPS_AFTER: 0, MergingOptions.FOCAL_POINT: 2}
        apply_time_merging(data, config)
        expected = pd.DataFrame(index=index)
        for _, df1 in data.items():
            df1.equals(expected)

    def test_apply_time_merging_multiple_dfs(self):
        data = {
            "First": create_data_frame(agents=2, times=3, columns=["A", "B"]),
            "Second": create_data_frame_with_additional_index_column(2, 4, ["A", "B"], "Plant"),
        }
        config = {MergingOptions.STEPS_BEFORE: 1, MergingOptions.STEPS_AFTER: 0, MergingOptions.FOCAL_POINT: 2}
        apply_time_merging(data, config)
        expected = {
            "First": pd.DataFrame.from_dict(
                {
                    (0, 0): {"A": 0, "B": 0},
                    (0, 2): {"A": 0, "B": 0},
                    (1, 0): {"A": 0, "B": 0},
                    (1, 2): {"A": 0, "B": 2 + 1},
                },
                orient="index",
            ).rename_axis(INDEX),
            "Second": pd.DataFrame.from_dict(
                {
                    (0, 0, "Plant"): {"A": 0, "B": 0},
                    (0, 2, "Plant"): {"A": 0, "B": 0},
                    (0, 4, "Plant"): {"A": 0, "B": 0},
                    (1, 0, "Plant"): {"A": 0, "B": 0},
                    (1, 2, "Plant"): {"A": 0, "B": 2 + 1},
                    (1, 4, "Plant"): {"A": 0, "B": 3},
                },
                orient="index",
            ).rename_axis(INDEX + ("Plant",)),
        }
        for (_, d), (_, e) in zip(data.items(), expected.items()):
            assert_frame_equal(d, e)
